import json
from channels.generic.websocket import AsyncWebsocketConsumer


class UpdateConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        code = self.scope['url_route']['kwargs'].get('code')
        await self.channel_layer.group_add(f'updates-{code}', self.channel_name)
        await self.accept()

    async def disconnect(self, code):
        code = self.scope['url_route']['kwargs'].get('code')
        await self.channel_layer.group_discard(f'updates-{code}', self.channel_name)

    async def receive(self, text_data=None, bytes_data=None):
        code = self.scope['url_route']['kwargs'].get('code')
        data = json.loads(text_data)

        if data['action'] == 'UPDATE':
            await self.channel_layer.group_send(f'updates-{code}',
                {
                    'type': 'broadcast_update',
                    'sender': self.channel_name,
                    'data': data
                }
            )

    async def broadcast_update(self, event):
        if event['sender'] == self.channel_name:
            return # Do not broadcast update to sender

        await self.send(text_data=json.dumps(event['data']))
