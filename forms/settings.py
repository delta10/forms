import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DEBUG_DEFAULT = 'True'
SECRET_KEY_DEFAULT = 'change-to-something-secret'

if os.getenv('FORMS_ENVIRONMENT') == 'production':
    DEBUG_DEFAULT = 'False'
    SECRET_KEY_DEFAULT = None

DEBUG = os.getenv('DEBUG', DEBUG_DEFAULT) == 'True'
SECRET_KEY = os.getenv('SECRET_KEY', SECRET_KEY_DEFAULT)

SESSION_COOKIE_SECURE = os.getenv('SESSION_COOKIE_SECURE', 'False') == 'True'
CSRF_COOKIE_SECURE = os.getenv('CSRF_COOKIE_SECURE', 'False') == 'True'

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

if CSRF_COOKIE_SECURE:
    # Do not set the cookie on SameSite as the form needs to be embeddable
    CSRF_COOKIE_SAMESITE = 'None'

if os.getenv('ALLOWED_HOST'):
    ALLOWED_HOSTS = [os.getenv('ALLOWED_HOST')]

OIDC_RP_CLIENT_ID = os.getenv('OIDC_CLIENT_ID', 'forms')
OIDC_RP_CLIENT_SECRET = os.getenv(
    'OIDC_CLIENT_SECRET', 'change-to-something-secret')
OIDC_RP_SIGN_ALGO = os.getenv('OIDC_SIGN_ALGO', 'RS256')
OIDC_RP_SCOPES = os.getenv('OIDC_SCOPES', 'openid email profile')
OIDC_OP_AUTHORIZATION_ENDPOINT = os.getenv(
    'OIDC_AUTHORIZATION_ENDPOINT', 'http://localhost:5556/auth')
OIDC_OP_TOKEN_ENDPOINT = os.getenv(
    'OIDC_TOKEN_ENDPOINT', 'http://localhost:5556/token')
OIDC_OP_USER_ENDPOINT = os.getenv(
    'OIDC_USER_ENDPOINT', 'http://localhost:5556/userinfo')
OIDC_OP_JWKS_ENDPOINT = os.getenv(
    'OIDC_JWKS_ENDPOINT', 'http://localhost:5556/keys')

INSTALLED_APPS = [
    'core',
    'webpack_loader',
    'mozilla_django_oidc',
    'reversion',
    'daphne',
    'channels',
    'rest_framework',
    'corsheaders',
    'django.forms',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAdminUser'
    ]
}

CORS_ALLOW_ALL_ORIGINS = True
CORS_URLS_REGEX = r'^/api/.*$'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

AUTH_USER_MODEL = 'core.User'

AUTHENTICATION_BACKENDS = [
    'core.auth.OIDCAuthBackend',
]

LOGIN_URL = '/oidc/authenticate/'

LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = os.getenv('LOGOUT_REDIRECT_URL', '/')

ROOT_URLCONF = 'forms.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'core.context_processors.user_menu',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

FORM_RENDERER = 'django.forms.renderers.TemplatesSetting'

WSGI_APPLICATION = 'forms.wsgi.application'
ASGI_APPLICATION = 'forms.asgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'HOST': os.getenv('DB_HOST', 'localhost'),
        'USER': os.getenv('DB_USER', 'forms'),
        'PASSWORD': os.getenv('DB_PASSWORD', 'forms'),
        'NAME': os.getenv('DB_NAME', 'forms'),
        'CONN_MAX_AGE': 600,
    }
}

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            'hosts': [f"redis://{os.getenv('REDIS_HOST', 'localhost')}/0"],
        },
    },
}

CELERY_BROKER_URL = f"redis://{os.getenv('REDIS_HOST', 'localhost')}/1"

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        }
    }
}

LANGUAGE_CODE = 'nl-NL'

TIME_ZONE = 'Europe/Amsterdam'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = '/app/static'

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.ManifestStaticFilesStorage'

# Save files using system's standard umask. This is required for network mounts like
# Azure Files as they do not implement a full-fledged permission system.
# See https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-FILE_UPLOAD_PERMISSIONS
FILE_UPLOAD_PERMISSIONS = None

WEBPACK_LOADER = {
    'DEFAULT': {
        'STATS_FILE': os.path.join(BASE_DIR, 'frontend', 'webpack-stats.json'),
    }
}

EMAIL_HOST = os.getenv('EMAIL_HOST')
EMAIL_PORT = os.getenv('EMAIL_PORT')
EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = os.getenv('EMAIL_USE_TLS', 'False') == 'True'
EMAIL_USE_SSL = os.getenv('EMAIL_USE_SSL', 'False') == 'True'
EMAIL_BACKEND = os.getenv(
    'EMAIL_BACKEND', 'django.core.mail.backends.smtp.EmailBackend')
DEFAULT_FROM_EMAIL = os.getenv(
    'DEFAULT_FROM_EMAIL', 'Forms <noreply@example.com>')

URL = os.getenv('URL', 'http://localhost:8000/')
