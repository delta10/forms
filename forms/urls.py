"""forms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import path, include, re_path
from django.views.static import serve
from rest_framework import routers

from core import views
from core import viewsets

admin.site.site_header = 'Forms beheer'
admin.site.enable_nav_sidebar = False

router = routers.DefaultRouter()
router.register(r'forms', viewsets.FormViewSet)
router.register(r'responses', viewsets.ResponseViewSet)

urlpatterns = [
    path('', views.index, name='dashboard'),
    path('forms/create', views.create_form, name='create_form'),
    path('forms/<int:form_id>', views.view_form, name='view_form'),
    path('forms/<int:form_id>/edit', views.edit_form, name='edit_form'),
    path('forms/<int:form_id>/duplicate', views.duplicate_form, name='duplicate_form'),
    path('forms/<int:form_id>/delete', views.delete_form, name='delete_form'),
    path('forms/<int:form_id>/introduction', views.introduction_form, name='introduction_form'),
    path('forms/<int:form_id>/introduction/<uuid:code>', views.introduction_form, name='introduction_form'),
    path('forms/<int:form_id>/request_access', views.request_access, name='request_access'),
    path('forms/<int:form_id>/fill', views.fill_form, name='fill_form'),
    path('forms/<int:form_id>/fill/<uuid:code>', views.fill_form, name='fill_form'),
    path('forms/<int:form_id>/embed', views.embed_form, name='embed_form'),
    path('forms/<int:form_id>/validate_email/<uuid:code>', views.validate_respondent_email, name='validate_respondent_email'),
    path('forms/<int:form_id>/preview', views.preview_form, name='preview_form'),
    path('forms/<int:form_id>/fill/download/xlsx', views.user_download_form_xlsx, name='user_download_form_xlsx'),
    path('forms/<int:form_id>/fill/download/xlsx/<int:response_id>', views.user_download_form_xlsx, name='user_download_form_xlsx'),
    path('forms/<int:form_id>/fill/download/docx', views.user_download_form_docx, name='user_download_form_docx'),
    path('forms/<int:form_id>/fill/download/docx/<int:response_id>', views.user_download_form_docx, name='user_download_form_docx'),
    path('forms/<int:form_id>/fill/download/pdf', views.user_download_form_pdf, name='user_download_form_pdf'),
    path('forms/<int:form_id>/fill/download/pdf/<int:response_id>', views.user_download_form_pdf, name='user_download_form_pdf'),
    path('forms/<int:form_id>/answers', views.form_answers, name='form_answers'),
    path('forms/<int:form_id>/respondents', views.form_respondents, name='form_respondents'),
    path('forms/<int:form_id>/respondents/create', views.form_respondents_create, name='form_respondents_create'),
    path('forms/<int:form_id>/actions', views.form_actions, name='form_actions'),
    path('forms/<int:form_id>/actions/create', views.form_actions_create, name='form_actions_create'),
    path('respondents/<int:respondent_id>/remind', views.remind_respondent, name='remind_respondent'),
    path('respondents/<int:respondent_id>/delete', views.delete_respondent, name='delete_respondent'),
    path('actions/<int:action_id>/edit', views.edit_action, name='edit_action'),
    path('actions/<int:action_id>/delete', views.delete_action, name='delete_action'),
    path('response/<int:response_id>/delete', views.delete_response, name='delete_response'),
    path('forms/<int:form_id>/export', views.admin_export_form, name='admin_export_form'),
    path('attachments/create', views.create_attachment, name='create_attachment'),
    path('oidc/', include('mozilla_django_oidc.urls')),
    path('api/v1/', include(router.urls)),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += [
        re_path(r'^media/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    ]
