# Forms

An enterprise solution for data collection

## Run forms locally
Make sure you installed [Docker](https://www.docker.com/) on your local machine.

Download or clone the repository. Then run the following command from the root of the repository:

```bash
docker-compose up
```

Browse to http://localhost:8000/. Login with admin@example.com / password.

The default settings can be used for testing purposes, but are not suitable for production usage.

Adjust docker-compose.yml to configure the application and create a secure production setup:

- DEBUG: [Django](https://https://www.djangoproject.com/) [debug mode](https://docs.djangoproject.com/en/3.0/ref/settings/#std:setting-DEBUG). (default: False)
- SECRET_KEY: Django [secret key](https://docs.djangoproject.com/en/3.0/ref/settings/#std:setting-SECRET_KEY). Replace with a generated password in production. (default: changemetosomethingsecret)
- ALLOWED_HOST: Django [allowed hosts](https://docs.djangoproject.com/en/3.0/ref/settings/#allowed-hosts) setting. The host that is allowed to serve the application. (default: localhost)
- SESSION_COOKIE_SECURE: Django [session cookie secure](https://docs.djangoproject.com/en/3.0/ref/settings/#std:setting-SESSION_COOKIE_SECURE) setting. (default: False)
- CSRF_COOKIE_SECURE: Django [CSRF cookie secure](https://docs.djangoproject.com/en/3.0/ref/settings/#std:setting-CSRF_COOKIE_SECURE) setting. (default: False)
- DB_HOST: The host of the [Postgres](https://https://www.postgresql.org/) database. (default: postgres)
- DB_USER: The username of the Postgres database. (default: forms)
- DB_PASSWORD: The password of the Postgres database. Replace with a generated password in production. (default: forms)
- DB_NAME: The database name of the Postgres database. (default: forms)
- OIDC_CLIENT_ID: The client id of the OpenID Connect provider (default: forms)
- OIDC_CLIENT_SECRET: The client secret of the OpenID Connect provider (default: change-to-something-secret)
- OIDC_AUTHORIZATION_ENDPOINT: The authorization endpoint of the OpenID Connect provider (default: http://localhost:5556/auth)
- OIDC_TOKEN_ENDPOINT: The token endpoint of the OpenID Connect provider (default: http://localhost:5556/token)
- OIDC_USER_ENDPOINT: The user endpoint of the OpenID Connect provider (default: http://localhost:5556/userinfo)
- OIDC_JWKS_ENDPOINT: The JWKS endpoint of the OpenID Connect provider (defualt: http://localhost:5556/keys)

## Setup a development environment
Make sure you installed the following requirements:

- [Python 3](https://www.python.org)
- [Docker](https://www.docker.com)
- [Node](https://nodejs.org/)

Start Postgres and Dex with:

```bash
docker-compose up -d postgres dex
```

Install the npm dependencies with:

```bash
(cd frontend/ && npm install)
```

Start the frontend development server with:

```bash
(cd frontend/ && npm run serve)
```

Setup a new virtual environment in another console with:

```bash
python -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
```

Now run the database migrations with:

```bash
python manage.py migrate
```

And start the backend development server with:

```bash
python manage.py runserver
```

## Testing

Run the unit tests for frontend and backend with:

```bash
(cd frontend/ && npm run test)
python manage.py test
```

## Contributing

Contributions are welcome, see [CONTRIBUTING.md](CONTRIBUTING.md) for more details. By contributing to this project, you accept and agree the the terms and conditions as specified in the [Contributor Licence Agreement](CLA.md).

## Licence

The software is distributed under the EUPLv1.2 licence, see the [LICENCE](LICENCE) file.
