from rest_framework import mixins, viewsets

from .models import Form, Response
from .serializers import FormSerializer, ResponseSerializer
from .permissions import ReadOnly, CreateOnly


class FormViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    permission_classes = [ReadOnly]
    queryset = Form.objects.filter(is_published=True, access=Form.Access.OPEN)
    serializer_class = FormSerializer


class ResponseViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    permission_classes = [CreateOnly]
    queryset = Response.objects.none()
    serializer_class = ResponseSerializer
