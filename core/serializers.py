from rest_framework import serializers

from .models import Form, Response
from .tasks import execute_actions, send_email_confirmation

class FormSerializer(serializers.ModelSerializer):
    class Meta:
        model = Form
        fields = ['id', 'title', 'is_response_downloadable', 'fields', 'is_closed', 'opening_date', 'closing_date', 'thank_you_message']


class ResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Response
        fields = ['id', 'form', 'data']

    def create(self, validated_data):
        response = super().create(validated_data)
        form = response.form

        execute_actions.delay(response.id)

        if form.email_confirmation_subject and form.email_confirmation_message:
            send_email_confirmation.delay(form.id, response.id)

        return response
