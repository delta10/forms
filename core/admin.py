from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from reversion.admin import VersionAdmin
from .models import Attachment, User, Form, Response, Respondent, Action


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    ordering = ['full_name']
    list_display = ['full_name', 'email', 'is_admin', 'last_login']
    search_fields = ['full_name', 'email']
    readonly_fields = ['full_name', 'email', 'last_login']
    fieldsets = (
        (None, {'fields': ['full_name', 'email', 'last_login']}),
        (_('Permissions'), {'fields': ['is_active', 'is_admin']})
    )


@admin.register(Form)
class FormAdmin(VersionAdmin):
    pass


@admin.register(Response)
class ResponseAdmin(VersionAdmin):
    pass

@admin.register(Respondent)
class RespondentAdmin(admin.ModelAdmin):
    pass


@admin.register(Attachment)
class AttachmentAdmin(admin.ModelAdmin):
    pass


@admin.register(Action)
class ActionAdmin(admin.ModelAdmin):
    list_display = ['form', 'key']
