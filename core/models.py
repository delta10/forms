import os
import uuid

from django.utils import timezone
from django.utils.crypto import get_random_string
from django.utils.functional import lazy
from django.utils.translation import gettext_lazy as _
from django.db.models import JSONField
from django.db import models
from django.core.exceptions import ValidationError
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
import reversion
from .lib import get_actions_from_apps
from .validators import validate_file_extension

MAX_URL_LENGTH = 2000


class UserManager(BaseUserManager):
    def create_user(self, full_name, email, password=None):
        if not full_name:
            raise ValueError('Users must have a full name')

        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            full_name=full_name,
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, full_name, email, password=None):
        user = self.create_user(
            full_name=full_name,
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.is_admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    full_name = models.CharField(max_length=255, db_index=True)
    email = models.EmailField(verbose_name='email address', unique=True)
    picture_url = models.CharField(
        max_length=MAX_URL_LENGTH, blank=True, default='')
    external_id = models.CharField(max_length=255, unique=True, null=True)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['full_name']

    class Meta:
        ordering = ['full_name']

    def __str__(self):
        return self.full_name

    def has_perm(self, perm, obj=None):
        # pylint: disable=unused-argument
        return True

    def has_module_perms(self, app_label):
        # pylint: disable=unused-argument
        return True

    @property
    def is_staff(self):
        return self.is_admin

    def to_dict(self):
        return {
            'id': self.id,
            'full_name': self.full_name
        }


def validate_fields(fields):  # pylint: disable=too-many-branches
    if not isinstance(fields, list):
        raise ValidationError('Moet een lijst van velden zijn')

    if len(fields) > 255:
        raise ValidationError('Aantal velden mag maximaal 255 zijn')

    last_heading = 1  # stands for h1

    for number, field in enumerate(fields, start=1):  # pylint: disable=too-many-nested-blocks
        if not isinstance(field, dict):
            raise ValidationError(f'Veld {number} is geen object')

        element = field.get('element')

        if element == 'field':
            for required_field in ['id', 'label']:
                if not field.get(required_field):
                    raise ValidationError(
                        f'Veld {number} heeft geen {required_field}')

            if field.get('type') == 'array':
                used_fields = []

                for sub_number, sub_field in enumerate(field.get('fields'), start=1):
                    for required_field in ['id', 'label']:
                        if not sub_field.get(required_field):
                            raise ValidationError(
                                f'Veld {number}.{sub_number} heeft geen {required_field}')

                    sub_field_id = sub_field.get('id')

                    if sub_field_id in used_fields:
                        raise ValidationError(
                            f'Veld {number}.{sub_number} bevat het interne id {sub_field_id}, maar deze wordt reeds gebruikt.')

                    used_fields.append(sub_field_id)

            continue

        if element in ['h2', 'h3', 'h4', 'p']:
            for required_field in ['value']:
                if not field.get(required_field):
                    raise ValidationError(
                        f'Veld {number} heeft geen {required_field}')

            if element in ['h2', 'h3', 'h4']:
                heading = int(element[1:])
                if heading > last_heading and (heading - 1) != last_heading:
                    raise ValidationError(
                        f'Kop {number}. Element h{heading} mag niet onder h{last_heading} vallen')

                last_heading = heading

            continue

        raise ValidationError(f'Veld {number} heeft onbekend type "{element}"')

    used_fields = []
    for number, field in enumerate(fields, start=1):
        field_id = field.get('id')

        if not field_id:
            continue

        if field.get('element') != 'field':
            continue

        if field_id in used_fields:
            raise ValidationError(
                f'Veld {number} bevat het interne id {field_id}, maar deze wordt reeds gebruikt.')

        used_fields.append(field_id)


def upload_to_logo(_, filename):
    dirname = timezone.now().strftime('logos/%Y/%m/%d/')
    return os.path.join(dirname, get_random_string(length=24), filename)


@reversion.register()
class Form(models.Model):
    class Access(models.TextChoices):
        OPEN = 'OPEN', _('Open')
        CLOSED = 'CLOSED', _('Gesloten')

    title = models.CharField(max_length=255)
    is_published = models.BooleanField(default=False)
    opening_date = models.DateTimeField(blank=True, null=True)
    closing_date = models.DateTimeField(blank=True, null=True)
    is_response_downloadable = models.BooleanField(default=True)
    respondent_can_request_access = models.BooleanField(default=False)
    respondent_can_preview_form = models.BooleanField(default=False)
    respondent_require_email_validation = models.BooleanField(default=False)
    fields = JSONField(default=list, blank=True, validators=[validate_fields])
    managers = models.ManyToManyField(
        'User', related_name='managed_forms', blank=True)
    access = models.CharField(
        max_length=20, choices=Access.choices, default=Access.OPEN)
    logo = models.ImageField(upload_to=upload_to_logo, blank=True, null=True)

    invitation_mail_subject = models.CharField(
        max_length=255, blank=True, null=True)
    invitation_mail_message = models.TextField(blank=True, null=True)

    introduction = models.TextField(blank=True, null=True)

    thank_you_message = models.TextField(blank=True, null=True)

    email_confirmation_subject = models.CharField(
        max_length=255, blank=True, null=True)
    email_confirmation_message = models.TextField(blank=True, null=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.title

    def is_closed(self):
        current_time = timezone.now()

        if self.opening_date and current_time < self.opening_date:
            return True
        if self.closing_date and current_time > self.closing_date:
            return True

        return False

    def get_fields(self):
        return [f for f in self.fields if f.get('element') == 'field']

    def to_dict(self):
        return {
            'id': self.pk,
            'title': self.title,
        }


@reversion.register()
class Response(models.Model):
    form = models.ForeignKey(
        'Form', on_delete=models.CASCADE, related_name='responses')
    data = JSONField(default=dict)
    respondent = models.ForeignKey(
        'Respondent', on_delete=models.SET_NULL, related_name='responses', null=True)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    class Meta:
        ordering = ['-id']

    def export_data(self):
        for field in self.form.get_fields():
            value = self.data.get(field.get('id'))

            if isinstance(value, dict) and value.get('_files'):
                yield list(Attachment.objects.filter(file__in=value.get('_files')))
            else:
                yield value


class Respondent(models.Model):
    form = models.ForeignKey(
        'Form', on_delete=models.CASCADE, related_name='respondents')
    full_name = models.CharField(max_length=255)
    phone = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(verbose_name='email address')
    code = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    last_notification = models.DateTimeField(
        auto_now_add=True, blank=True, null=True)

    def __str__(self):
        return f'{self.full_name} <{self.email}>'

    def to_dict(self):
        return {
            'id': self.pk,
            'full_name': self.full_name,
            'email': self.email,
            'phone': self.phone,
            'code': self.code,
            'last_notification': self.last_notification,
            'has_responded': self.responses.count() > 0
        }


def upload_to_attachment(_, filename):
    dirname = timezone.now().strftime('attachments/%Y/%m/%d/')
    return os.path.join(dirname, get_random_string(length=24), filename)


class Attachment(models.Model):
    response = models.ForeignKey(
        'Response', on_delete=models.CASCADE, related_name='attachments', null=True, blank=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    file = models.FileField(upload_to=upload_to_attachment, validators=[
                            validate_file_extension])

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if not self.name:
            self.name = os.path.basename(self.file.name)

        super().save(force_insert=force_insert,
                     force_update=force_update, using=using, update_fields=update_fields)

    def __str__(self):
        return self.name


class Action(models.Model):
    form = models.ForeignKey(
        'Form', on_delete=models.CASCADE, related_name='actions')
    key = models.CharField(max_length=255, choices=lazy(
        get_actions_from_apps, tuple)(), verbose_name=_('type'))
    args = JSONField(default=dict, blank=True, verbose_name=_('instellingen'))

    def __str__(self):
        return f'{self.form}, {self.key}'
