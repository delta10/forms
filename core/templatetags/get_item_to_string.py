from django.template.defaulttags import register

@register.filter
def get_item_to_string(input_dictionary, key):
    value = input_dictionary.get(key)

    if isinstance(value, dict) and value.get('_files'):
        file_count = len(value.get('_files'))
        return f'{file_count} bijlage(s)'

    return str(value)
