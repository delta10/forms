from importlib import import_module
from urllib.parse import urljoin, urlencode
from email.utils import formataddr
from celery import shared_task
from django.conf import settings
from django.core.mail import EmailMessage
from django.template import Context, Template
from django.template.loader import render_to_string
from django.urls import reverse
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from .models import Response, User, Form, Respondent


@shared_task
def execute_actions(response_id):
    response = Response.objects.get(id=response_id)

    for action in response.form.actions.all():
        mod = import_module(action.key)
        cls = getattr(mod, 'Action')
        instance = cls()
        instance.execute(response, {**action.args})


@shared_task
def send_invitation(form_id, respondent_id, sender_id):
    form = Form.objects.get(id=form_id)
    respondent = Respondent.objects.get(id=respondent_id)
    sender = User.objects.get(id=sender_id)

    variables = {
        'form': form,
        'respondent': respondent,
        'invite_url': urljoin(settings.URL, reverse('introduction_form', args=[form.id, respondent.code]))
    }

    if form.invitation_mail_subject:
        subject = Template(form.invitation_mail_subject).render(
            Context(variables))
    else:
        subject = render_to_string('email/invitation_subject.txt', variables)

    if form.invitation_mail_message:
        message = Template(form.invitation_mail_message).render(
            Context(variables))
    else:
        message = render_to_string('email/invitation_message.txt', variables)

    email = EmailMessage(
        subject,
        message,
        None,
        [formataddr((respondent.full_name, respondent.email))],
        [],
        reply_to=[formataddr((sender.full_name, sender.email))],
    )

    return email.send()


@shared_task
def send_email_confirmation(form_id, response_id):
    form = Form.objects.get(id=form_id)
    response = Response.objects.get(id=response_id)

    if not form.email_confirmation_subject:
        return False

    if not form.email_confirmation_message:
        return False

    try:
        validate_email(response.data.get('email'))
    except ValidationError:
        return False

    variables = {
        'form': form,
        'response': response
    }

    subject = Template(form.email_confirmation_subject).render(
        Context(variables))
    message = Template(form.email_confirmation_message).render(
        Context(variables))

    email = EmailMessage(
        subject,
        message,
        None,
        [formataddr((response.data.get('name'), response.data.get('email')))],
        []
    )

    if email.send():
        return True

    return False


@shared_task
def send_request_access_message(form_id, full_name, email, phone):
    form = Form.objects.get(id=form_id)

    base_url = urljoin(settings.URL, reverse('form_respondents_create', args=[form.id]))

    query_params = urlencode({
        'full_name': full_name,
        'email': email,
        'phone': phone if phone else ''
    })

    variables = {
        'form': form,
        'full_name': full_name,
        'email': email,
        'phone': phone,
        'respondents_create_url': f'{base_url}?{query_params}'
    }

    subject = render_to_string('email/request_access_subject.txt', variables)
    message = render_to_string('email/request_access_message.txt', variables)

    email = EmailMessage(
        subject,
        message,
        None,
        [formataddr((manager.full_name, manager.email)) for manager in form.managers.all()],
        [],
        reply_to=[formataddr((full_name, email))],
    )

    if email.send():
        return True

    return False


@shared_task
def send_validate_email(form_id, respondent_id, code, token):
    form = Form.objects.get(id=form_id)
    respondent = Respondent.objects.get(id=respondent_id)

    base_url = urljoin(settings.URL, reverse('validate_respondent_email', args=[form.id, code]))
    query_params = urlencode({
        'token': token,
    })

    variables = {
        'form': form,
        'respondent': respondent,
        'validate_email_url': f'{base_url}?{query_params}'
    }

    subject = render_to_string('email/validate_email_subject.txt', variables)
    message = render_to_string('email/validate_email_message.txt', variables)

    email = EmailMessage(
        subject,
        message,
        None,
        [formataddr((respondent.full_name, respondent.email))],
        []
    )

    if email.send():
        return True

    return False