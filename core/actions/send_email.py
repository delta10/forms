import logging
from django.conf import settings
from django.template import Context, Template
from urllib.parse import urljoin
from smtplib import SMTPException
from django.utils.text import slugify
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from django.urls import reverse
from core.export import export_pdf

logger = logging.getLogger(__name__)


class Action:
    name = 'Verstuur e-mail'

    def execute(self, response, args):
        variables = {
            'form': response.form,
            'response': response,
            'show_view_form_url': args.get('show_view_form_url', True),
            'view_form_url': urljoin(settings.URL, reverse('view_form', args=[response.form.id]))
        }

        if args.get('subject'):
            subject_template = Template(args.get('subject'))
            subject = subject_template.render(Context(variables))
        else:
            subject = render_to_string('email/notify_on_new_response_subject.txt', variables)

        if args.get('message'):
            message_template = Template(args.get('message'))
            message = message_template.render(Context(variables))
        else:
            message = render_to_string('email/notify_on_new_response_message.txt', variables)

        try:
            email = EmailMessage(
                subject,
                message,
                args.get('email_from', None),
                args['email_to']
            )

            if args.get('email_cc') and args['email_cc'] != ['']:
                email.cc = args['email_cc']

            if args.get('email_bcc') and args['email_bcc'] != ['']:
                email.bcc = args['email_bcc']

            if args.get('reply_to'):
                email.reply_to = [ args.get('reply_to') ]

            if args.get('reply_to_field') and response.data.get(args.get('reply_to_field')):
                email.reply_to = [ response.data.get(args.get('reply_to_field')) ]

            if args.get('add_attachments'):
                try:
                    filename = f'{slugify(response.form.title)}.pdf'
                    email.attach(filename, export_pdf(response.form, response), 'application/pdf')
                except ValueError:
                    pass

                for attachment in response.attachments.all():
                    try:
                        email.attach_file(attachment.file.path)
                    except ValueError:
                        pass

            return email.send()

        except (SMTPException, ConnectionRefusedError):
            logger.error('Could not send e-mail due to connection error', exc_info=True)
            return False
