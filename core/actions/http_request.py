import requests
from requests.exceptions import RequestException


class Action:
    name = 'HTTP verzoek'

    def execute(self, response, args):
        try:
            http_response = requests.post(args['url'], data={
                'response': response.data
            }, timeout=10)
            http_response.raise_for_status()
            return True
        except RequestException:
            return False
