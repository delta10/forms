import io
import json
import os
from urllib.parse import urljoin
import zipfile
from django.conf import settings
from querystring_parser import parser
from datetime import timedelta
from django.middleware import csrf
from django.core import signing
from django.core.exceptions import ValidationError
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone, formats
from django.utils.text import slugify
from django.views.decorators.clickjacking import xframe_options_exempt
from webpack_loader.utils import get_files
from uuid import uuid4
from docx import Document
from docx.shared import Inches
from openpyxl import Workbook
import reversion
from .export import export_pdf
from .lib import get_current_timezone_formatted
from .models import Attachment, Form, Response, Respondent, Action, User
from .forms import CreateActionForm, EditActionForm, FormForm, RespondentForm, DuplicateFormForm
from .tasks import execute_actions, send_invitation, send_email_confirmation, send_request_access_message, send_validate_email


@login_required
def index(request):
    return render(request, 'index.html', {
        'title': 'Overzicht',
        'background': 'gray',
        'forms': Form.objects.filter(managers=request.user),
    })


@login_required
def create_form(request):
    if request.method == 'POST':
        with reversion.create_revision():
            form = FormForm(request.POST)

            if form.is_valid():
                created_form = form.save()
                created_form.managers.add(request.user)

                reversion.set_user(request.user)
                reversion.set_comment('Create form')

                return redirect('dashboard')

    else:
        form = FormForm()

    return render(request, 'create_form.html', {
        'title': 'Nieuw formulier',
        'form': form,
        'js_editor': {
            'initialFields': []
        }
    })


@login_required
def edit_form(request, form_id):
    form = get_object_or_404(Form, pk=form_id)

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    if request.method == 'POST':
        with reversion.create_revision():
            form_form = FormForm(request.POST, request.FILES, instance=form)

            if form_form.is_valid():
                created_form = form_form.save()
                created_form.managers.add(request.user)

                reversion.set_user(request.user)
                reversion.set_comment('Update form')

                return redirect('view_form', form.id)
    else:
        form_form = FormForm(instance=form)

    return render(request, 'edit_form.html', {
        'title': 'Formulier wijzigen',
        'form_id': form_id,
        'form': form_form,
        'current_timezone': get_current_timezone_formatted(),
        'js_editor': {
            'initialFields': form.fields
        },
        'js_manager_selector': {
            'initialManagers': [manager.to_dict() for manager in form.managers.all()],
            'allUsers': [user.to_dict() for user in User.objects.all()],
            'currentUser': request.user.to_dict()
        },
        'js_introduction_editor_data': {
            'csrftoken': csrf.get_token(request),
            'initialIntroduction': form.introduction
        }
    })


@login_required
def duplicate_form(request, form_id):
    form = get_object_or_404(Form, pk=form_id)

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    if request.method == 'POST':
        with reversion.create_revision():
            duplicateform_form = DuplicateFormForm(request.POST)

            if duplicateform_form.is_valid():
                cleaned_data = duplicateform_form.cleaned_data

                duplicated_form = Form.objects.get(pk=form_id)
                duplicated_form.pk = None
                duplicated_form.title = cleaned_data['title']
                duplicated_form.save()

                duplicated_form.managers.add(request.user)
                reversion.set_user(request.user)
                reversion.set_comment('Duplicate form')

                if cleaned_data['copy_respondents']:
                    respondent_mapping = {}

                    for respondent in form.respondents.all():
                        old_respondent_id = respondent.pk

                        respondent.pk = None
                        respondent.form = duplicated_form
                        respondent.code = uuid4()
                        respondent.save()

                        respondent.last_notification = None
                        respondent.save()

                        respondent_mapping[old_respondent_id] = respondent

                    if cleaned_data['copy_answers']:
                        for response in form.responses.all():
                            response.pk = None
                            response.form = duplicated_form

                            if response.respondent:
                                response.respondent = respondent_mapping[response.respondent.id]

                            response.save()

                if cleaned_data['copy_actions']:
                    for action in form.actions.all():
                        action.pk = None
                        action.form = duplicated_form
                        action.save()

                return redirect('view_form', duplicated_form.id)
    else:
        i = 2
        while Form.objects.filter(managers=request.user, title=f'{form.title} ({i})').count() > 0:
            i += 1

        initial_data = {
            'title': f'{form.title} ({i})'
        }

        duplicateform_form = DuplicateFormForm(initial_data)

    return render(request, 'duplicate_form.html', {
        'title': 'Formulier dupliceren',
        'form_id': form_id,
        'form': duplicateform_form,
        'js_editor': {
            'initialFields': form.fields
        }
    })


@login_required
def delete_form(request, form_id):
    form = get_object_or_404(Form, pk=form_id)

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    if request.method == 'POST':
        form.delete()
        return redirect('dashboard')

    return render(request, 'delete_form.html', {
        'title': 'Formulier verwijderen',
        'form_id': form_id,
        'form': form,
    })


@login_required
def view_form(request, form_id):
    form = get_object_or_404(Form, pk=form_id)

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    return render(request, 'view_form.html', {
        'title': form.title,
        'background': 'gray',
        'form': form,
        'fields': [f for f in form.fields if f.get('element') == 'field']
    })


@xframe_options_exempt
def introduction_form(request, form_id, code=None):
    form = get_object_or_404(Form, pk=form_id)

    if not form.introduction:
        if code:
            return redirect('fill_form', form_id, code)

        return redirect('fill_form', form_id)

    if not form.is_published and request.user not in form.managers.all():
        raise Http404('Form is not published')

    return render(request, 'introduction_form.html', {
        'form': form,
        'code': code,
        'js_introduction_data': {
            'form_id': form.id,
            'introduction': form.introduction,
            'show_button': form.access == Form.Access.OPEN or code is not None,
            'code': code
        }
    })


@xframe_options_exempt
def request_access(request, form_id):
    form = get_object_or_404(Form, pk=form_id)

    if not form.is_published and request.user not in form.managers.all():
        raise Http404('Form is not published')

    if form.access != Form.Access.CLOSED:
        raise Http404('Form is not closed')

    if not form.respondent_can_request_access:
        raise Http404('Not able to request access for form')

    if request.method == 'POST':
        respondent_form = RespondentForm(request.POST)

        if respondent_form.is_valid():
            send_request_access_message.delay(
                form.id,
                respondent_form.cleaned_data['full_name'],
                respondent_form.cleaned_data['email'],
                respondent_form.cleaned_data['phone']
            )

            return render(request, 'request_access_success.html', {
                'form': form,
            })

    else:
        respondent_form = RespondentForm()

    return render(request, 'request_access.html', {
        'form': form,
        'respondent_form': respondent_form,
    })


@xframe_options_exempt
def fill_form(request, form_id, code=None):  # pylint:disable=too-many-branches
    form = get_object_or_404(Form, pk=form_id)

    submitted = False
    respondent = None
    response = None

    if not form.is_published and request.user not in form.managers.all():
        raise Http404('Form is not published')

    if form.access == Form.Access.CLOSED:
        if not code:
            if request.user not in form.managers.all():
                raise Http404('Respondent does not exist')

        if code:
            try:
                respondent = form.respondents.get(code=code)
                if form.respondent_require_email_validation and request.user not in form.managers.all():
                    if request.session.get('validated_email') != respondent.email:
                        return redirect('validate_respondent_email', form.id, code)
            except Respondent.DoesNotExist as does_not_exist:
                raise Http404('Respondent does not exist') from does_not_exist

        if respondent:
            try:
                response, _ = form.responses.get_or_create(
                    respondent=respondent)
            except Response.DoesNotExist:
                pass

    if request.method == 'PATCH':
        if not response:
            return HttpResponseBadRequest('Cannot patch with an unknown response')

        try:
            with reversion.create_revision():
                response.data = {**response.data, **
                                 _get_form_data(form, json.loads(request.body))}
                response.save()

                if request.user.is_authenticated:
                    reversion.set_user(request.user)

                reversion.set_comment('Updated response data')
        except ValueError:
            return HttpResponseBadRequest('Cannot decode JSON')

        return JsonResponse({
            'data': response.data
        })

    if request.method == 'POST':
        with reversion.create_revision():
            if not response:
                response = Response()

            raw_data = parser.parse(request.POST.urlencode())

            response.form = form
            response.data = _get_form_data(form, raw_data)

            if respondent:
                response.respondent = respondent

            response.save()

            if request.user.is_authenticated:
                reversion.set_user(request.user)

            reversion.set_comment('Saved response data')

        request.session['filled_response'] = response.id
        submitted = True

        execute_actions.delay(response.id)

        if form.email_confirmation_subject and form.email_confirmation_message:
            send_email_confirmation.delay(form.id, response.id)

    return render(request, 'fill_form.html', {
        'title': form.title,
        'form': form,
        'code': code,
        'js_form_data': {
            'id': form.id,
            'title': form.title,
            'logo': form.logo.url if form.logo else None,
            'toc': _get_table_of_contents_data(form),
            'code': code,
            'fields': form.fields,
            'isClosed': form.is_closed(),
            'openingDate': form.opening_date,
            'closingDate': form.closing_date,
            'response': response.data if response else {},
            'csrftoken': csrf.get_token(request),
            'respondent': str(respondent) if respondent else None
        },
        'respondent': respondent,
        'response': response,
        'submitted': submitted,
        'closed': form.is_closed(),
    })

def embed_form(request, form_id):
    form = get_object_or_404(Form, pk=form_id)

    if not form.is_published and request.user not in form.managers.all():
        raise Http404('Form is not published')

    if form.access == Form.Access.CLOSED and request.user not in form.managers.all():
        raise Http404('Respondent does not exist')

    chunks = []
    for chunk in get_files('embed'):
        chunk_path = urljoin(settings.URL, chunk['url'])
        if chunk['name'].endswith('.js'):
            chunks.append(f'<script src="{chunk_path}"></script>')
        elif chunk['name'].endswith('.css'):
            chunks.append(f'<link href="{chunk_path}" rel="stylesheet" />')

    return render(request, 'embed.html', {
        'api_url': urljoin(settings.URL, 'api/v1'),
        'form': form,
        'chunks': chunks
    })


@xframe_options_exempt
def validate_respondent_email(request, form_id, code=None):
    form = get_object_or_404(Form, pk=form_id)

    respondent = None

    if not form.is_published and request.user not in form.managers.all():
        raise Http404('Form is not published')

    if not Form.Access.CLOSED:
        raise Http404('Form is not closed')

    if not code:
        raise Http404('No code provided')

    try:
        respondent = form.respondents.get(code=code)
    except Respondent.DoesNotExist as does_not_exist:
        raise Http404('Respondent does not exist') from does_not_exist

    signer = signing.TimestampSigner()

    error = None
    sent = None

    if request.GET.get('token'):
        try:
            signed_data = signer.unsign_object(request.GET.get('token'), max_age=timedelta(minutes=15))
            request.session['validated_email'] = signed_data['email']
            return redirect('fill_form', form_id, code)
        except signing.SignatureExpired:
            error = 'De link is verlopen. Vraag een nieuwe link aan door op de knop te klikken.'
        except signing.BadSignature:
            error = 'De link is ongeldig. Vraag een nieuwe link aan door op de knop te klikken.'

    if request.method == 'POST':
        token = signer.sign_object({
            'email': respondent.email
        })

        send_validate_email.delay(form.id, respondent.id, code, token)
        sent = True

    return render(request, 'validate_respondent_email.html', {
        'respondent': respondent,
        'form': form,
        'error': error,
        'sent': sent
    })

@xframe_options_exempt
def preview_form(request, form_id):
    form = get_object_or_404(Form, pk=form_id)

    if not form.is_published and request.user not in form.managers.all():
        raise Http404('Form is not published')

    if form.access == Form.Access.CLOSED and not form.respondent_can_preview_form:
        raise Http404('Form cannot be previewed')

    return render(request, 'fill_form.html', {
        'title': form.title,
        'form': form,
        'js_form_data': {
            'title': form.title,
            'logo': form.logo.url if form.logo else None,
            'toc': _get_table_of_contents_data(form),
            'fields': form.fields,
            'response': {},
            'preview': True
        },
    })


@xframe_options_exempt
def user_download_form_xlsx(request, form_id, response_id=None):
    form = get_object_or_404(Form, pk=form_id)

    if not form.is_response_downloadable:
        raise Http404('Form does not exist')

    response = _get_response_from_id_or_session(request, form, response_id)

    columns = [
        'Vraag',
        'Antwoord'
    ]

    workbook = Workbook()
    worksheet = workbook.active
    worksheet.append(columns)
    worksheet.column_dimensions['A'].width = 50
    worksheet.column_dimensions['B'].width = 60

    i = 0
    for field in form.fields:
        if field.get('element') == 'field':
            i += 1

            if field.get('type') == 'array':
                worksheet.append(
                    [f"{i}. {field['label']}"] +
                    [subfield.get('label') for subfield in field.get('fields')]
                )

                for value in response.data.get(field['id']).values():
                    worksheet.append(
                        [''] +
                        [value.get(_field_to_string(subfield.get('id')))
                         for subfield in field.get('fields')]
                    )
            else:
                worksheet.append([
                    f"{i}. {field['label']}",
                    _field_to_string(response.data.get(field['id']))
                ])

        elif field.get('element') == 'h2':
            if i == 0:
                worksheet.title = field['value'][:30]
            else:
                worksheet = workbook.create_sheet(field['value'][:30])
                worksheet.append(columns)
                worksheet.column_dimensions['A'].width = 50
                worksheet.column_dimensions['B'].width = 60

    http_response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    http_response['Content-Disposition'] = f'attachment; filename={slugify(form.title)}.xlsx'

    workbook.save(http_response)

    return http_response


@xframe_options_exempt
def user_download_form_docx(request, form_id, response_id=None):
    #pylint: disable-msg=too-many-locals
    form = get_object_or_404(Form, pk=form_id)

    if not form.is_response_downloadable:
        raise Http404('Form does not exist')

    response = _get_response_from_id_or_session(request, form, response_id)

    document = Document()
    document.add_heading(form.title, 0)

    if form.logo:
        document.add_picture(form.logo, width=Inches(3))

    if response.respondent:
        document.add_paragraph(
            f'Respondent: {response.respondent.full_name} ({response.respondent.email})'
        )

    document.add_paragraph(
        f"""Aangemaakt op: {formats.localize(response.created_at)}\n"""
        f"""Laatst bijgewerkt: {formats.localize(response.updated_at)}"""
    )

    document.add_page_break()

    for field in form.fields:
        if field.get('element') == 'field':
            content = response.data.get(field['id'])

            if field.get('type') == 'array':
                document.add_paragraph(
                )
                table = document.add_table(
                    rows=1, cols=len(field.get('fields')))
                hdr_cells = table.rows[0].cells

                for i, subfield in enumerate(field.get('fields')):
                    hdr_cells[i].text = subfield.get('label')

                for row in content:
                    row_cells = table.add_row().cells

                    for i, subfield in enumerate(field.get('fields')):
                        row_cells[i].text = content[row].get(
                            subfield.get('id'))

            else:
                paragraph = document.add_paragraph()
                paragraph.add_run(f"{field['label']}\n").bold = True
                paragraph.add_run(_field_to_string(content))
        elif field.get('element') == 'h2':
            document.add_heading(field['value'], 1)
        elif field.get('element') == 'h3':
            document.add_heading(field['value'], 2)
        elif field.get('element') == 'h4':
            document.add_heading(field['value'], 3)
        elif field.get('element') == 'p':
            document.add_paragraph(field['value'])

    http_response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
    http_response['Content-Disposition'] = f'attachment; filename={slugify(form.title)}.docx'

    document.save(http_response)

    return http_response


@xframe_options_exempt
def user_download_form_pdf(request, form_id, response_id=None):
    form = get_object_or_404(Form, pk=form_id)

    if not form.is_response_downloadable and request.user not in form.managers.all():
        raise Http404('Form does not exist')

    response = _get_response_from_id_or_session(request, form, response_id)

    http_response = HttpResponse(
        export_pdf(form, response),
        content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
    http_response['Content-Disposition'] = f'attachment; filename={slugify(form.title)}.pdf'

    return http_response


@login_required
def form_answers(request, form_id):
    form = get_object_or_404(Form, pk=form_id)

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    return render(request, 'form_answers.html', {
        'title': form.title,
        'background': 'gray',
        'form': form,
        'fields': [f for f in form.fields if f.get('element') == 'field']
    })


@login_required
def form_respondents(request, form_id):
    form = get_object_or_404(Form, pk=form_id)

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    return render(request, 'form_respondents.html', {
        'title': form.title,
        'background': 'gray',
        'form': form,
        'js_respondentList': {
            'form': form.to_dict(),
            'respondents': [respondent.to_dict() for respondent in form.respondents.all()],
            'csrftoken': csrf.get_token(request),
        }
    })


@login_required
def form_respondents_create(request, form_id):
    form = get_object_or_404(Form, pk=form_id)

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    if request.method == 'POST':
        respondent_form = RespondentForm(request.POST)

        if respondent_form.is_valid():
            respondent = respondent_form.save(commit=False)
            respondent.form = form
            respondent.save()

            if respondent_form.cleaned_data['send_email_notification']:
                send_invitation.delay(form.id, respondent.id, request.user.id)

            return redirect('form_respondents', form.id)
    else:
        initial_data = None
        if request.GET.get('full_name') and request.GET.get('email'):
            initial_data = {
                'full_name': request.GET.get('full_name'),
                'email': request.GET.get('email'),
                'phone': request.GET.get('phone')
            }

        respondent_form = RespondentForm(initial_data)

    return render(request, 'form_respondents_create.html', {
        'title': form.title,
        'background': 'gray',
        'form': form,
        'respondent_form': respondent_form,
    })


@login_required
def form_actions(request, form_id):
    form = get_object_or_404(Form, pk=form_id)

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    return render(request, 'form_actions.html', {
        'title': form.title,
        'background': 'gray',
        'form': form,
    })


@login_required
def form_actions_create(request, form_id):
    form = get_object_or_404(Form, pk=form_id)

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    if request.method == 'POST':
        action_form = CreateActionForm(request.POST)

        if action_form.is_valid():
            action = action_form.save(commit=False)
            action.form = form
            action.save()

            return redirect('form_actions', form.id)
    else:
        action_form = CreateActionForm()

    return render(request, 'form_actions_create.html', {
        'title': form.title,
        'background': 'gray',
        'form': form,
        'action_form': action_form,
        'js_actions_editor': {
            'initialValues': {
                'key': '',
                'args': {}
            }
        }
    })


@login_required
def admin_export_form(request, form_id):
    #pylint: disable-msg=too-many-locals
    form = get_object_or_404(Form, pk=form_id)
    fields = [f for f in form.fields if f.get('element') == 'field']

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    workbook = Workbook()
    answers_worksheet = workbook.active
    answers_worksheet.title = 'Antwoorden'

    columns = ['ID', 'Datum']

    if form.access == Form.Access.CLOSED:
        columns += ['Respondent']

    columns += [field.get('id') for field in fields]
    answers_worksheet.append(columns)

    summary_worksheet = workbook.create_sheet('Samenvatting')
    summary_worksheet.title = 'Samenvatting'

    attachments = []
    summary = {}

    for form_response in form.responses.all():
        row = [form_response.id, form_response.created_at]

        if form.access == Form.Access.CLOSED:
            row += [str(form_response.respondent)]

        values = []
        for field in fields:
            value = form_response.data.get(field.get('id'), '')
            values.append(_field_to_string(value))

            if field.get('id') in summary:
                summary[field.get('id')].append(value)
            else:
                summary[field.get('id')] = [value]

            if isinstance(value, dict) and value.get('_files'):
                for attachment in value.get('_files'):
                    try:
                        attachments.append(Attachment.objects.get(file=attachment))
                    except Attachment.DoesNotExist:
                        pass

        row += values
        answers_worksheet.append(row)

    for field in fields:
        if field.get('type') == 'number':
            pretty_value = sum(float(value) if _is_float(
                value) else 0 for value in summary[field.get('id')])
        else:
            pretty_value = _field_to_string(summary[field.get('id')])

        summary_worksheet.append([field.get('id'), pretty_value])

    xlsx_in_memory = io.BytesIO()
    workbook.save(xlsx_in_memory)

    zip_in_memory = io.BytesIO()

    with zipfile.ZipFile(zip_in_memory, "w") as zf:
        zf.writestr(
            f'{slugify(form.title)}.xlsx',
            xlsx_in_memory.getvalue()
        )

        for attachment in attachments:
            try:
                zf.write(
                    attachment.file.path,
                    arcname=os.path.join(
                        'attachments',
                        f'{str(attachment.id)}-{os.path.basename(attachment.file.name)}'
                    )
                )
            except FileNotFoundError:
                pass

    response = HttpResponse(zip_in_memory.getvalue(),
                            content_type='application/x-zip-compressed')
    response['Content-Disposition'] = f'attachment; filename={slugify(form.title)}.zip'

    return response


@login_required
def remind_respondent(request, respondent_id):
    respondent = get_object_or_404(Respondent, pk=respondent_id)
    form = respondent.form

    if not form:
        raise Http404('Form does not exist')

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    if request.method == 'POST':
        send_invitation.delay(form.id, respondent.id, request.user.id)
        respondent.last_notification = timezone.now()
        respondent.save()

        return redirect('form_respondents', form.id)

    return render(request, 'remind_respondent.html', {
        'title': 'Respondent herinneren',
        'respondent': respondent,
    })


@login_required
def delete_respondent(request, respondent_id):
    respondent = get_object_or_404(Respondent, pk=respondent_id)
    form = respondent.form

    if not form:
        raise Http404('Form does not exist')

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    if request.method == 'POST':
        respondent.delete()
        return redirect('form_respondents', form.id)

    return render(request, 'delete_respondent.html', {
        'title': 'Respondent verwijderen',
        'respondent': respondent,
    })


def delete_response(request, response_id):
    response = get_object_or_404(Response, pk=response_id)
    form = response.form

    if not form:
        raise Http404('Form does not exist')

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    if request.method == 'POST':
        response.delete()
        return redirect('form_answers', form.id)

    return render(request, 'delete_response.html', {
        'title': 'Antwoord verwijderen',
        'response': response,
    })


@login_required
def edit_action(request, action_id):
    action = get_object_or_404(Action, pk=action_id)
    form = action.form

    if not form:
        raise Http404('Form does not exist')

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    if request.method == 'POST':
        action_form = EditActionForm(request.POST, instance=action)

        if action_form.is_valid():
            action = action_form.save()
            return redirect('form_actions', form.id)
    else:
        action_form = EditActionForm(instance=action)

    return render(request, 'edit_action.html', {
        'title': 'Actie wijzigen',
        'action': action,
        'action_form': action_form,
        'js_actions_editor': {
            'initialValues': {
                'key': action.key,
                'args': action.args
            }
        }
    })


@login_required
def delete_action(request, action_id):
    action = get_object_or_404(Action, pk=action_id)
    form = action.form

    if not form:
        raise Http404('Form does not exist')

    if request.user not in form.managers.all():
        raise Http404('Form does not exist')

    if request.method == 'POST':
        action.delete()
        return redirect('form_actions', form.id)

    return render(request, 'delete_action.html', {
        'title': 'Actie verwijderen',
        'action': action,
    })


def _get_form_data(form, raw_data):
    data = {}

    valid_fields = [field.get('id')
                    for field in form.fields if field.get('id') and field.get('type') != 'file']

    for key, value in raw_data.items():
        if key in valid_fields:
            data[key] = value

    valid_file_fields = [field.get('id')
                    for field in form.fields if field.get('id') and field.get('type') == 'file']

    for key, value in raw_data.items():
        if key in valid_file_fields:
            if isinstance(value, str):
                data[key] = {'_files': [value]}
            else:
                data[key] = {'_files': value}

    return data


def _get_table_of_contents_data(form):
    HEADINGS = ['h2', 'h3']

    table_of_contents = []

    for field in form.fields:
        if field.get('element') in HEADINGS:
            table_of_contents.append(
                {
                    'element': field.get('element'),
                    'value': field.get('value')
                }
            )

    return table_of_contents


def _is_float(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


def _get_response_from_id_or_session(request, form, response_id):
    if response_id:
        if request.user not in form.managers.all():
            raise Http404('Response does not exist')

        return get_object_or_404(form.responses, pk=response_id)

    try:
        return get_object_or_404(form.responses, pk=request.session['filled_response'])
    except KeyError as key_not_set:
        raise Http404('Could not retrieve response') from key_not_set


def _field_to_string(value):
    if isinstance(value, dict) and value.get('_files'):
        file_count = len(value.get('_files'))
        return f'{file_count} bijlage(s)'

    return str(value).replace('\r', '')


def create_attachment(request):
    saved_attachments = []

    for fieldname in request.FILES:
        for uploaded_file in request.FILES.getlist(fieldname):
            try:
                attachment = Attachment(file=uploaded_file)
                attachment.full_clean()
                attachment.save()

                saved_attachments.append({
                    'name': attachment.name,
                    'filename': attachment.file.name,
                    'url': attachment.file.url
                })
            except ValidationError:
                pass

    return JsonResponse({
        'attachments': saved_attachments
    })
