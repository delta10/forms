import os
from urllib.parse import urljoin
from django.urls import reverse
from fpdf import FPDF, HTMLMixin
from django.template.loader import render_to_string
from django.conf import settings

def export_pdf(form, response):
    class PDF(FPDF, HTMLMixin):
        pass

    context = {
        'form': form,
        'response': response,
        'view_form_url': urljoin(settings.URL, reverse('view_form', args=[form.id]))
    }

    pdf = PDF(font_cache_dir=None)
    pdf.add_font('Roboto', '', os.path.join(settings.BASE_DIR, 'fonts/Roboto-Regular.ttf'), uni=True)
    pdf.add_font('Roboto', 'B', os.path.join(settings.BASE_DIR, 'fonts/Roboto-Bold.ttf'), uni=True)
    pdf.add_font('Roboto', 'I', os.path.join(settings.BASE_DIR, 'fonts/Roboto-Italic.ttf'), uni=True)
    pdf.add_font('Roboto', 'BI', os.path.join(settings.BASE_DIR, 'fonts/Roboto-BoldItalic.ttf'), uni=True)
    pdf.set_font('Roboto', '', 14)

    pdf.set_title(form.title)
    pdf.set_lang(settings.LANGUAGE_CODE)

    pdf.add_page()
    pdf.write_html(render_to_string('export/pdf_cover.html', context))

    pdf.add_page()
    pdf.write_html(render_to_string('export/pdf_fields.html', context))

    return bytes(pdf.output())
