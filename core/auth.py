from mozilla_django_oidc.auth import OIDCAuthenticationBackend


class OIDCAuthBackend(OIDCAuthenticationBackend):
    def filter_users_by_claims(self, claims):
        sub = claims.get('sub')
        if not sub:
            return self.UserModel.objects.none()

        return self.UserModel.objects.filter(external_id__iexact=sub)

    def create_user(self, claims):
        return self.UserModel.objects.create(
            full_name=claims.get('name', ''),
            email=claims.get('email'),
            picture_url=claims.get('picture', ''),
            external_id=claims.get('sub')
        )

    def update_user(self, user, claims):
        user.full_name = claims.get('name', '')
        user.email = claims.get('email', '')
        user.picture_url = claims.get('picture', '')
        user.save()
        return user
