from django import forms
from django.forms.widgets import DateTimeInput

from .models import Form, Respondent, Action


class FormForm(forms.ModelForm):
    class Meta:
        model = Form
        fields = [
            'title',
            'logo',
            'access',
            'is_published',
            'opening_date',
            'closing_date',
            'is_response_downloadable',
            'respondent_can_request_access',
            'respondent_can_preview_form',
            'respondent_require_email_validation',
            'fields',
            'managers',
            'introduction',
            'invitation_mail_subject',
            'invitation_mail_message',
            'thank_you_message',
            'email_confirmation_subject',
            'email_confirmation_message',
        ]

    # format the date string in a way that `<input type="datetime-local" />` expects
    opening_date = forms.DateTimeField(widget=DateTimeInput(format='%Y-%m-%dT%H:%M'), required=False)
    closing_date = forms.DateTimeField(widget=DateTimeInput(format='%Y-%m-%dT%H:%M'), required=False)


class RespondentForm(forms.ModelForm):
    send_email_notification = forms.BooleanField(required=False, initial=True)

    class Meta:
        model = Respondent
        fields = ['full_name', 'email', 'phone']


class CreateActionForm(forms.ModelForm):
    class Meta:
        model = Action
        fields = ['key', 'args']


class EditActionForm(forms.ModelForm):
    class Meta:
        model = Action
        fields = ['args']


class DuplicateFormForm(forms.Form):
    title = forms.CharField(label='Titel', max_length=100)
    copy_respondents = forms.BooleanField(
        label='Kopieer respondenten', required=False)
    copy_answers = forms.BooleanField(
        label='Kopieer antwoorden', required=False)
    copy_actions = forms.BooleanField(label='Kopieer acties', required=False)
