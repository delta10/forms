# Generated by Django 3.0.6 on 2020-05-27 11:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20200526_1850'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='form',
            name='slug',
        ),
        migrations.AddField(
            model_name='response',
            name='form',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, to='core.Form'),
            preserve_default=False,
        ),
    ]
