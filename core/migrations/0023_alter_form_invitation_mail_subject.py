# Generated by Django 3.2 on 2021-06-28 19:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0022_auto_20210628_2138'),
    ]

    operations = [
        migrations.AlterField(
            model_name='form',
            name='invitation_mail_subject',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
    ]
