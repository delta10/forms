# Generated by Django 3.0.6 on 2020-05-26 18:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20200525_0751'),
    ]

    operations = [
        migrations.AddField(
            model_name='form',
            name='slug',
            field=models.SlugField(default=None, max_length=100),
            preserve_default=False,
        ),
    ]
