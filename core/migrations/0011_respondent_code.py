# Generated by Django 3.0.6 on 2020-06-22 18:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_respondent_form'),
    ]

    operations = [
        migrations.AddField(
            model_name='respondent',
            name='code',
            field=models.CharField(default=None, max_length=32, unique=True),
            preserve_default=False,
        ),
    ]
