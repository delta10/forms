# Generated by Django 3.0.6 on 2020-05-25 07:51

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20200525_0748'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='response',
            name='respondent',
        ),
        migrations.AddField(
            model_name='form',
            name='managers',
            field=models.ManyToManyField(related_name='managed_forms', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='response',
            name='respondents',
            field=models.ManyToManyField(related_name='responses', to='core.Respondent'),
        ),
    ]
