from django.middleware.csrf import get_token

def user_menu(request):
    return {
        'js_user_menu_data': {
            'csrftoken': get_token(request),
            'user': format_user(request.user)
        }
    }

def format_user(user):
    if user.is_anonymous:
        return {}

    return {
        'id': user.id,
        'fullName': user.full_name,
        'pictureUrl': user.picture_url
    }