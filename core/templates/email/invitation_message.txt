Beste {{ respondent.full_name }},

Graag willen we je uitnodigen om het formulier "{{ form.title }}" in te vullen. Dit kan via de volgende link:

{{ invite_url }}
