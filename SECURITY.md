# Security

This document describes the security aspects of the project.

## Authentication / authorisation

Authentication is done through an external OpenID Connect provider. Authorisation is handled inside the application. System-wide administrators use the admin panel. Form administrators authorize co-administrators per form.

## Login procedure

The user logs in through OpenID Connect. Two-factor authentication can be enforced at the identity provider.

## Password policy

Not applicable as this is handled at the identity provider.

## Cryptography policy

No passsword hashes are stored, this is handled at the identity provider. SSL / TLS encryption is handled on Kubernetes Ingress level. Encryption at rest can be handled by the storage provider in Kubernetes.

## Backup policy

For the hosted version there is no backup policy in place yet as the system is not into production. In production we will create a backup every four hours with a retention of 30 days.
