#!/usr/bin/env sh
set -e

# Collect static
python manage.py collectstatic --noinput

# Run migrations
python manage.py migrate

# Start webserver for proxy and static assets
nginx

# Start Daphne asgi server
daphne -u /run/daphne/daphne.sock --proxy-headers forms.asgi:application
