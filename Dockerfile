# Frontend
FROM node:18.14.0-alpine AS frontend-build
WORKDIR /app/frontend

COPY frontend/package.json \
    frontend/package-lock.json \
    /app/frontend/
RUN npm install

COPY . /app
RUN npm run build

# Backend
FROM python:3.9-slim-bullseye AS backend-build

RUN apt-get update && apt-get install --no-install-recommends -y \
    libpq-dev \
    build-essential

RUN python -m venv /app/venv && /app/venv/bin/pip install --upgrade pip

WORKDIR /app
COPY requirements.txt /app
RUN /app/venv/bin/pip3 install -r requirements.txt

# Final
FROM python:3.9-slim-bullseye

RUN apt-get update && apt-get install --no-install-recommends -y \
    libpq5 \
    nginx \
    mime-support \
    && rm -rf /var/lib/apt/lists/*

COPY --from=backend-build /app/venv /app/venv
ENV PATH="/app/venv/bin:${PATH}"

COPY --from=frontend-build /app/core/static/dist /app/core/static/dist
COPY --from=frontend-build /app/frontend/webpack-stats.json /app/frontend/webpack-stats.json

COPY . /app

COPY ./docker/default /etc/nginx/sites-available/default
COPY ./docker/start.sh /
RUN chmod +x /start.sh

RUN mkdir -p /app/static /app/media && chown www-data:www-data /app/static /app/media

RUN touch /run/nginx.pid && chown -R www-data /var/lib/nginx /run/nginx.pid
RUN mkdir /run/daphne && chown www-data /run/daphne

ENV PYTHONUNBUFFERED 1
ENV FORMS_ENVIRONMENT production

WORKDIR /app
EXPOSE 8000
CMD ["/start.sh"]
USER www-data
