const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  future: {
    purgeLayersByDefault: true,
  },
  purge: {
    content: ['./src/**/*.vue', './src/**/*.js', '../core/templates/**/*.html'],
  },
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
      outline: {
        blue: ['2px solid #3182CE', '2px'],
      },
    },
    typography: {
      default: {
        css: {
          color: '#333',
          a: {
            color: '#1c64f2',
            '&:hover': {
              color: '#2c5282',
            },
          },
        },
      },
    },
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
    },
  },
  plugins: [require('@tailwindcss/ui')],
};
