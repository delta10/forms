const BundleTracker = require('webpack-bundle-tracker');

const DEPLOYMENT_PATH = '/static/dist/';

module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? DEPLOYMENT_PATH
      : 'http://localhost:8080/',
  outputDir: '../core/static/dist',

  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  },

  configureWebpack: {
    plugins: [
      new BundleTracker({ path: __dirname, filename: 'webpack-stats.json' }),
    ],
    output: {
      filename: '[name].js',
    },
  },

  css: {
    sourceMap: true,
    extract: {
      filename: '[name].css',
      chunkFilename: '[name]-chunk.css',
    },
  },

  pages: {
    app: {
      entry: 'src/main.js',
    },
    embed: {
      entry: 'src/embed.js',
    },
  },
};
