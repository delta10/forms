import { extend } from 'vee-validate';
import { setInteractionMode } from 'vee-validate';
import { required, email, min_value, max_value } from 'vee-validate/dist/rules';

setInteractionMode('eager');

extend('required', {
  ...required,
  message: "Het veld '{_field_}' is verplicht",
});

extend('email', {
  ...email,
  message: "Het veld '{_field_}' is niet geldig",
});

extend('min_value', {
  ...min_value,
  message: "Het veld '{_field_}' moet groter of gelijk zijn aan {min}",
});

extend('max_value', {
  ...max_value,
  message: "Het veld '{_field_}' moet kleiner of gelijk zijn aan {max}",
});
