import Vue from 'vue';
import ManagerSelector from './components/ManagerSelector';

document.addEventListener('DOMContentLoaded', () => {
  const el = document.querySelector('#js-manager-selector-entry-point');
  const data = document.querySelector('#js-manager-selector-data');
  if (!el || !data) return;

  new Vue({
    el: '#js-manager-selector-entry-point',
    render: (h) =>
      h(ManagerSelector, {
        props: JSON.parse(data.innerHTML),
      }),
  });
});
