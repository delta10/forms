import './main.css';
import './validation-rules';

import 'core-js/es/object/values';
import 'es6-promise/auto';
import 'whatwg-fetch';

import Vue from 'vue';
import FormComponent from './form/components/FormComponent';

const Form = class {
  constructor(target, settings) {
    this.renderForm(target, settings);
  }

  async renderForm(target, settings) {
    const baseUrl = settings.apiUrl ? settings.apiUrl : '/api/v1';

    try {
      const result = await fetch(`${baseUrl}/forms/${settings.formId}/`);
      const form = await result.json();

      new Vue({
        el: target,
        render: (h) =>
          h(FormComponent, {
            props: {
              id: form.id,
              baseUrl: baseUrl,
              embed: true,
              title: form.title,
              fields: form.fields,
              thankYouMessage: form.thank_you_message,
              response: {},
              isClosed: form.is_closed,
              openingDate: form.opening_date,
              closingDate: form.closing_date,
            },
          }),
      });
    } catch (e) {
      console.error(e);
    }
  }
};

window.forms = {
  Form,
};

window.addEventListener('DOMContentLoaded', () => {
  if (window.initForm) {
    window.initForm();
  }
});
