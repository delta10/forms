import Vue from 'vue';
import IntroductionEditor from './components/IntroductionEditor';

document.addEventListener('DOMContentLoaded', () => {
  const el = document.querySelector('#js-introduction-editor-entry-point');
  const data = document.querySelector('#js-introduction-editor-data');
  if (!el || !data) return;

  new Vue({
    el: '#js-introduction-editor-entry-point',
    render: (h) =>
      h(IntroductionEditor, {
        props: JSON.parse(data.innerHTML),
      }),
  });
});
