import './main.css';
import './validation-rules';

import 'core-js/es/object/values';
import 'es6-promise/auto';
import 'whatwg-fetch';

import Vue from 'vue';
import Vuex from 'vuex';

Vue.config.productionTip = false;
Vue.use(Vuex);

import './editor';
import './actions-editor';
import './form';
import './user-menu';
import './manager-selector';
import './introduction';
import './introduction-editor';
import './respondent-list';
