import Vue from 'vue';
import UserMenu from './components/UserMenu';

document.addEventListener('DOMContentLoaded', () => {
  const el = document.querySelector('#js-user-menu-entry-point');
  const data = document.querySelector('#js-user-menu-data');
  if (!el || !data) return;

  new Vue({
    el: '#js-user-menu-entry-point',
    render: (h) =>
      h(UserMenu, {
        props: JSON.parse(data.innerHTML),
      }),
  });
});
