import Vue from 'vue';
import Introduction from './components/Introduction';

document.addEventListener('DOMContentLoaded', () => {
  const el = document.querySelector('#js-introduction-entry-point');
  const data = document.querySelector('#js-introduction-data');
  if (!el || !data) return;

  new Vue({
    el: '#js-introduction-entry-point',
    render: (h) =>
      h(Introduction, {
        props: JSON.parse(data.innerHTML),
      }),
  });
});
