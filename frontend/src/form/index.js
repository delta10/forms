import Vue from 'vue';
import FormComponent from './components/FormComponent';

document.addEventListener('DOMContentLoaded', () => {
  const el = document.querySelector('#js-form-entry-point');
  const data = document.querySelector('#js-form-data');
  if (!el || !data) return;

  new Vue({
    el: '#js-form-entry-point',
    render: (h) =>
      h(FormComponent, {
        props: JSON.parse(data.innerHTML),
      }),
  });

  const settings = JSON.parse(data.innerHTML);

  let reconnectTimer;
  const followUpdates = async () => {
    const websocketProtocol =
      window.location.protocol === 'https:' ? 'wss:' : 'ws:';
    const url = `${websocketProtocol}//${window.location.host}/ws/forms/1/updates/${settings.code}`;

    const updateSocket = new WebSocket(url);
    updateSocket.onmessage = (e) => {
      const data = JSON.parse(e.data);

      if (data.action === 'UPDATE') {
        document.querySelector(`#${data.id}`).value = data.value;
      }
    };

    updateSocket.onerror = () => {
      updateSocket.close();
    };

    updateSocket.onopen = () => {
      console.log('Connected to update socket.');

      clearInterval(reconnectTimer);

      document.querySelectorAll('input, textarea, select').forEach((field) => {
        field.onchange = (e) => {
          const data = {
            action: 'UPDATE',
            id: e.target.id,
            value: e.target.value,
          };

          updateSocket.send(JSON.stringify(data));
        };
      });

      updateSocket.onclose = () => {
        console.error('Update socket closed.');
        reconnectTimer = setInterval(() => {
          console.log('Trying to reconnect.');
          followUpdates();
        }, 5000);
      };
    };
  };

  if (settings.code) {
    followUpdates();
  }

  const form = document.querySelector('#form');
  form.addEventListener('submit', () => {
    window.parent.postMessage('forms:event:form-submitted', '*');
  });
});
