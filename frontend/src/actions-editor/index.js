import Vue from 'vue';
import ActionsEditor from './components/ActionsEditor';

document.addEventListener('DOMContentLoaded', () => {
  const el = document.querySelector('#js-actions-editor-entry-point');
  const data = document.querySelector('#js-actions-editor-data');
  if (!el || !data) return;

  new Vue({
    el: '#js-actions-editor-entry-point',
    render: (h) =>
      h(ActionsEditor, {
        props: JSON.parse(data.innerHTML),
      }),
  });
});
