import Vue from 'vue';
import UserMenu from './components/RespondentList';

document.addEventListener('DOMContentLoaded', () => {
  const el = document.querySelector('#js-respondentList-entry-point');
  const data = document.querySelector('#js-respondentList-data');
  if (!el || !data) return;

  new Vue({
    el: '#js-respondentList-entry-point',
    render: (h) =>
      h(UserMenu, {
        props: JSON.parse(data.innerHTML),
      }),
  });
});
