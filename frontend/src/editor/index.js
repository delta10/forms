import Vue from 'vue';
import EditorComponent from './components/EditorComponent';

document.addEventListener('DOMContentLoaded', () => {
  const el = document.querySelector('#js-editor-entry-point');
  const data = document.querySelector('#js-editor-data');
  if (!el || !data) return;

  new Vue({
    el: '#js-editor-entry-point',
    render: (h) =>
      h(EditorComponent, {
        props: JSON.parse(data.innerHTML),
      }),
  });
});
